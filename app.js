var express = require('express'),
    path = require('path'),
    bp = require('body-parser'),
    cons = require('consolidate'),
    dust = require('dustjs-helpers'),
    pg = require('pg'),
    sessions = require('client-sessions'),
    app = express(),
    server = require('http').createServer(app),
    io = require('socket.io').listen(server);

server.listen(3773, function(){
    console.log('Server started on port 3773')
});

var conStr = "postgres://mypls:11@localhost/myplsdb";
app.engine('dust', cons.dust);
app.set('view engine', 'dust');
app.set('views', __dirname + '/views');
app.use(express.static(path.join(__dirname, 'public')));

app.use(bp.json());
app.use(bp.urlencoded({ extended: false}));
app.use(sessions({
    cookieName: 'session',
    secret: 'esfandyarikaurmaxwelsingh',
    duration: 10 * 60 * 1000,
    activeDuration: 5 * 60 * 1000,
}));

//----------------------- Post sign in ------------------------//

app.post('/signin', function(req, res){
    var loginid = req.body.loginid;
    if (loginid == "admin") {
        req.session.user = loginid;
        res.redirect('/admin');
        return;
    }
    const { Client } = require('pg');

    const client = new Client({
        connectionString: conStr
    });

    client.connect(function (err) {
        if (err) {
            return console.error('error fetching client from pool', err);
        }
    });


    client.query('SELECT uname FROM users WHERE uuid=$1',
        [loginid], function (err, result) {
            if (err) {
                return console.error('error running select login', err);
            }
            if (result.rowCount == 0) {
                return;
            }
            req.session.user = loginid;
            res.redirect('/user');
        });
});

function requireLogin (req, res, next) {
    if (!req.session || !req.session.user) {
        res.redirect('/');
    } else if (req.session.user == "admin") {
        res.redirect('/admin');
    } else {
        next();
    }
};

function requireAdminLogin (req, res, next) {
    if (!req.session || !req.session.user) {
        res.redirect('/login');
    } else if (req.session.user != "admin") {
        res.redirect('/login');
    } else {
        next();
    }
};

app.get('/logout', function(req, res) {
    req.session.reset();
    res.redirect('/login');
});

app.get('/', function(req, res){
    res.redirect('/login');
});

app.get('/login', function(req, res){
    res.render('index');
});

app.get('/admin', requireAdminLogin, function(req, res){
    const { Client } = require('pg');

    const client = new Client({
        connectionString: conStr
    });

     client.connect(function (err) {
         if (err) {
             return console.error('error fetching client from pool', err);
         }
     });

    client.query('SELECT * FROM users', function (err, result) {
        if (err) {
            return console.error('error running query for users', err);
        }
        client.query('SELECT * FROM topics', function (err, restopics) {
            if (err) {
                return console.error('error running query for topics', err);
            }
            res.render('admin', {users: result.rows, topics:restopics.rows});
        });
    });
});

app.post('/add', function(req, res){
    const { Client } = require('pg');

    const client = new Client({
        connectionString: conStr
    });

    var addedUser;

    client.connect(function (err) {
        if (err) {
            return console.error('error fetching client from pool', err);
        }
    });

    client.query('INSERT INTO users(uname) VALUES($1)',
        [req.body.uname], function (err, result) {
            if (err) {
                return console.error('error running insert query', err);
            }
            client.query('SELECT * FROM users ORDER BY uuid DESC LIMIT 1', function (err, resultuser) {
                if (err) {
                    return console.error('error running insert query', err);
                }
                var selTopics = [];
                if (req.body.seltopic instanceof Array) {
                    for (var i = 0; i < req.body.seltopic.length; i++) {
                        selTopics.push(req.body.seltopic[i]);
                    }
                } else {
                    selTopics.push(req.body.seltopic);
                }
                client.query('INSERT INTO userlocal(uuid, t_ids, my_gids, uname) VALUES($1, $2, $3, $4)',
                    [resultuser.rows[0].uuid, selTopics, [], req.body.uname], function (err, result) {
                        if (err) {
                            return console.error('error running insert userlocal', err);
                        }
                        res.redirect('admin');
                    });
            });
        });
});

/********************************** Dashboard ************************************/

app.get('/user', requireLogin, function(req, res){
    const { Client } = require('pg');

    const client = new Client({
        connectionString: conStr
    });

    client.connect(function (err) {
        if (err) {
            return console.error('error fetching client from pool', err);
        }
    });

    client.query('SELECT * FROM users WHERE uuid=$1',
        [req.session.user], function (err, result) {
        if (err) {
            return console.error('error running query for username', err);
        }
        res.render('user', {userinfo:result.rows});
    });
});

/********************************** Chat ************************************/

app.get('/chat', requireLogin, function(req, res){
    const { Client } = require('pg');

    const client = new Client({
        connectionString: conStr
    });

    client.connect(function (err) {
        if (err) {
            return console.error('error fetching client from pool', err);
        }
    });
    client.query('SELECT * FROM users WHERE uuid=$1',
        [req.session.user], function (err, resultusers) {
            if (err) {
                return console.error('error running query for username', err);
            }
            client.query('SELECT * FROM userlocal WHERE uuid=$1',
                [req.session.user], function (err, resultlocal) {
                if (err) {
                    return console.error('error running query for topics', err);
                }
                var groupList = [];
                client.query('SELECT g_id, gname FROM mygroups', function (err, resultgid) {
                    if (err) {
                        return console.error('error running select query', err);
                    }
                    var gids = resultlocal.rows[0].my_gids;
                    if (0 != gids.length) {
                        for (var i = 0; i < gids.length; i++) {
                            for (var j = 0; j < resultgid.rows.length; j++){
                                if (gids[i] == resultgid.rows[j].g_id) {
                                    var data = {
                                        gid: resultgid.rows[j].g_id,
                                        gname: resultgid.rows[j].gname
                                    };
                                    groupList.push(data);
                                }
                            }
                        }
                    }
                    res.render('chat', {topics:resultlocal.rows[0].t_ids, userinfo:resultusers.rows, groups:groupList});
                });
            });
    });
});

//-----------Select Topic for creating Chat Room----------------
app.post('/selecttopic', function(req, res){
    const { Client } = require('pg');

    const client = new Client({
        connectionString: conStr
    });

    client.connect(function (err) {
        if (err) {
            return console.error('error fetching client from pool', err);
        }
    });

    var members = [];
    members.push(req.session.user);
    client.query('INSERT INTO mygroups(gname, t_id, admin, members) VALUES($1, $2 , $3, $4)',
        [req.body.gname,req.body.topicselect,req.session.user, members], function (err, result) {
        if (err) {
            return console.error('error running insert query', err);
        }
        client.query('SELECT * FROM mygroups ORDER BY g_id DESC LIMIT 1', function (err, resultgid) {
            if (err) {
                return console.error('error running select query', err);
            }
            UpdateUserGroups(client, req.session.user, resultgid.rows[0].g_id);
        });
    });

    client.query('SELECT uuid, t_ids, uname  FROM userlocal ', function (err, result) {
        if (err) {
            return console.error('error running query for topics', err);
        }
        var i, j;
        var interestedusers = [];

        for (i=0;i<result.rows.length;i++){
            if(!result.rows[i].t_ids) continue;
            for(j=0;j<result.rows[i].t_ids.length;j++){
                if(req.body.topicselect == result.rows[i].t_ids[j]) {
                    if (result.rows[i].uuid != req.session.user) {
                        var data = {
                            uuid: result.rows[i].uuid,
                            users: result.rows[i].uname
                        };
                        interestedusers.push(data);
                    }
                }
            }
        }
        res.render('chat', {userlocal:interestedusers});
    });
});

//-------------- Select Member For creating Chat Room-----
app.post('/selectmember', function(req, res){
    const { Client } = require('pg');

    const client = new Client({
        connectionString: conStr
    });

    client.connect(function (err) {
        if (err) {
            return console.error('error fetching client from pool', err);
        }
    });

    client.query('SELECT * FROM userlocal WHERE uuid=$1',
        [req.session.user], function (err, resultuser) {
            if (err) {
                return console.error('error running query for userlocal', err);
            }
            if(0 == resultuser.rows.length) return;
            var mygids = resultuser.rows[0].my_gids;
            var frm_name = resultuser.rows[0].uname;
            var gid = mygids[mygids.length - 1];
            var members = [];
            if (req.body.memberselect instanceof Array) {
                for (var i = 0; i < req.body.memberselect.length; i++) {
                    members.push(req.body.memberselect[i]);
                }
            } else {
                members.push(req.body.memberselect);
            }
            if (0 == members.length){
                return;
            }
            for (var i = 0; i < members.length; i++) {
                InsertNotifications(client, req.session.user, members[i], "invite", gid, frm_name);
            }
            res.redirect('/chat');
        });
});

app.post('/reqjoin', requireLogin, function(req, res){
    const { Client } = require('pg');

    const client = new Client({
        connectionString: conStr
    });

    client.connect(function (err) {
        if (err) {
            return console.error('error fetching client from pool', err);
        }
    });

    client.query('SELECT * FROM mygroups WHERE g_id=$1',
        [req.body.join], function (err, resultgrp) {
            if (err) {
                return console.error('error running query for userlocal', err);
            }
            if(0 == resultgrp.rows.length) return;
            client.query('SELECT * FROM userlocal WHERE uuid=$1',
                [req.session.user], function (err, resultuser) {
                    if (err) {
                        return console.error('error running query for userlocal', err);
                    }
                    var frm_name = resultuser.rows[0].uname;
                    InsertNotifications(client, req.session.user, resultgrp.rows[0].admin, "reqjoin", req.body.join, frm_name);
                    res.status(200);
                });
        });
});

function InsertNotifications(client, frm_uuid, to_uuid, type, gid, frm_name){
    client.query('SELECT gname FROM mygroups WHERE g_id=$1', [gid], function (err, resultgrp) {
        if (err) {
            return console.error('error running select query', err);
        }
        if (0 == resultgrp.rows.length) return;
        var grpname = resultgrp.rows[0].gname;
        client.query('INSERT INTO notif (fr_uuid, to_uuid, type, gid, fr_name, gp_name)' +
            'VALUES ($1, $2, $3, $4, $5, $6) ',
            [frm_uuid, to_uuid, type, gid, frm_name, grpname], function (err, result) {
                if (err) {
                    return console.error('error running insert query', err);
                }
            });
    });
}

function UpdateUserGroups(client, uuid, gid) {
    client.query('SELECT * FROM userlocal WHERE uuid=$1',
        [uuid], function (err, resultuser) {
        if (err) {
            return console.error('error running query for userlocal', err);
        }
        if(0 == resultuser.rows.length) return;
        var mygids = resultuser.rows[0].my_gids;
        mygids.push(gid);
        client.query('UPDATE userlocal SET my_gids=$1 WHERE uuid=$2',
            [mygids, uuid], function (err, result) {
                if (err) {
                    return console.error('error running insert query', err);
                }
            });
    });
}

/********************************** Share ************************************/

app.get('/share', requireLogin, function(req, res){
    const { Client } = require('pg');

    const client = new Client({
        connectionString: conStr
    });

    client.connect(function (err) {
        if (err) {
            return console.error('error fetching client from pool', err);
        }
    });

    client.query('SELECT * FROM userlocal WHERE uuid=$1',
        [req.session.user], function (err, resultlocal) {
            if (err) {
                return console.error('error running query for topics', err);
            }
            var groupList = [];
            client.query('SELECT g_id, gname FROM mygroups', function (err, resultgid) {
                if (err) {
                    return console.error('error running select query', err);
                }
                var gids = resultlocal.rows[0].my_gids;
                if (0 != gids.length) {
                    for (var i = 0; i < gids.length; i++) {
                        for (var j = 0; j < resultgid.rows.length; j++){
                            if (gids[i] == resultgid.rows[j].g_id) {
                                var data = {
                                    gid: resultgid.rows[j].g_id,
                                    gname: resultgid.rows[j].gname
                                };
                                groupList.push(data);
                            }
                        }
                    }
                }
                res.render('share', {groups:groupList});
            });
        });
});

/********************************** Questions ************************************/

app.get('/questions', requireLogin, function(req, res){
    res.render('questions');
});


/*********************************Courses+UserInterest*************************/
app.post('/query',function(req,res) {

    const {Client} = require('pg');

    const client = new Client({
        connectionString: conStr
    });
    client.connect(function (err) {
        if (err) {
            return console.error('error fetching client from pool', err);
        }
    });
    //console.log(req.body.searchinterest);
    client.query('SELECT * FROM userlocal ', function (err, result) {
        if (err) {
            return console.error('error running query for topics', err);
        }
        var i, j;
        var interestedUsers = [];

        for (i = 0; i < result.rows.length; i++) {
            if (!result.rows[i].t_ids) continue;
            for (j = 0; j < result.rows[i].t_ids.length; j++) {
                if (req.body.searchinterest == result.rows[i].t_ids[j]) {

                    interestedUsers.push(result.rows[i].uuid + ' ' + result.rows[i].uname);

                }
            }
        }
        client.query('SELECT * FROM mygroups where t_id=$1',[req.body.searchinterest],function(err,resultgroups) {
            if (err) {
                return console.error('error running insert query', err);
            }
            client.query('SELECT * FROM courses where t_name=$1',
                [req.body.searchinterest], function (err, resultcourses) {
                    if (err) {
                        return console.error('error running insert query', err);
                    }
                    res.render('searchresult', {userlocal: interestedUsers,mygroups:resultgroups.rows,courses: resultcourses.rows});
                });
        });
    });
});



/*********************************delete*************************************/
app.delete('/delete/:uuid', function(req, res){
    const { Client } = require('pg');

    const client = new Client({
        connectionString: conStr
    });

    client.connect(function (err) {
        if (err) {
            return console.error('error fetching client from pool', err);
        }
    });

    client.query('DELETE FROM users WHERE uuid=$1',
        [req.params.uuid], function (err) {
        if (err) {
            return console.error('error running query to delete user', err);
        }
            client.query('DELETE FROM userlocal WHERE uuid=$1',
                [req.params.uuid], function (err) {
                    if (err) {
                        return console.error('error running query to delete userlocal', err);
                    }
                });
        res.sendStatus(200);
    });
});







/****************************************** Online chat ***************************************************/

app.post('/enterchat', function(req, res){
    //req.session
    req.session.gid = req.body.g_id;
    res.redirect('/chatPage');
});

app.get('/chatPage', function(req, res){
    const { Client } = require('pg');

    const client = new Client({
        connectionString: conStr
    });

    client.connect(function (err) {
        if (err) {
            return console.error('error fetching client from pool', err);
        }
    });

    client.query('SELECT * FROM users WHERE uuid=$1',
        [req.session.user], function (err, result) {
            if (err) {
                return console.error('error running query for username', err);
            }
            var groupid = [];
            var data = {
                gid: req.session.gid
            };
            groupid.push(data);
            res.render('chatPage', {userinfo:result.rows, group:groupid});
        });
});

var usernames = {};

io.sockets.on('connection', function(socket){
    console.log('Socket Connected...');

    //for new user
    socket.on('new user', function(data, gid, callback){
        /*if(usernames.indexOf(data) != -1){
            callback(false);
        } else */{
            callback(true);
            socket.username = data;
            socket.gid = gid;
            if (usernames.hasOwnProperty(gid)) {
                usernames[gid].push(socket.username);
            } else {
                var arr = [];
                arr.push(socket.username);
                usernames[gid] = arr;
            }
            updateUsernames(gid);
            socket.join(gid);
        }
    });

    // Update Usernames
    function updateUsernames(gid){
        io.sockets.emit('usernames', usernames[gid]);
    }

    // Send Message
    socket.on('send message', function(data){
        io.sockets.in(socket.gid).emit('new message', {msg: data, user:socket.username});
    });

    // Disconnect
    socket.on('disconnect', function(data){
        if(!socket.username){
            return;
        }

        usernames[socket.gid].splice(usernames[socket.gid].indexOf(socket.username), 1);
        updateUsernames(socket.gid);
    });

});


/******************************** Notification *****************************/

app.get('/notif', requireLogin, function(req, res){
    const { Client } = require('pg');

    const client = new Client({
        connectionString: conStr
    });

    client.connect(function (err) {
        if (err) {
            return console.error('error fetching client from pool', err);
        }
    });

    client.query('SELECT * FROM notif WHERE to_uuid=$1', [req.session.user], function (err, result) {
        if (err) {
            return console.error('error running query for users', err);
        }
        var notiftxt = [];
        for (var i = 0; i < result.rows.length; i++) {
            var msg;
            if (result.rows[i].type == 'invite') {
                msg = "Invitation to join group " + result.rows[i].gp_name + " from " + result.rows[i].fr_name;
            } else if (result.rows[i].type == 'reqjoin') {
                msg = "Request to join group " + result.rows[i].gp_name + " from " + result.rows[i].fr_name;
            }
            var data = {
                notif_id: result.rows[i].id,
                notif_txt: msg
            };
            notiftxt.push(data);
        }
        res.render('notif', {notifs: notiftxt});
    });
});

app.post('/notifbtn', function(req, res) {
    const {Client} = require('pg');

    const client = new Client({
        connectionString: conStr
    });

    client.connect(function (err) {
        if (err) {
            return console.error('error fetching client from pool', err);
        }
    });

    var str = req.body.event;
    var event = str.match(/^[a-z]+/);
    var notifid = str.match(/\d+$/);

    if ("accept" == event[0]) {
        client.query('SELECT * FROM notif WHERE id=$1',
            [notifid[0]], function (err, resultnotif) {
                if (err) {
                    return console.error('error running query for groups', err);
                }
                if (0 == resultnotif.rows.length) return;
                if ("invite" == resultnotif.rows[0].type) {
                    UpdateGroupTable(client, resultnotif.rows[0].to_uuid, resultnotif.rows[0].gid);
                    UpdateUserGroups(client, resultnotif.rows[0].to_uuid, resultnotif.rows[0].gid);
                } else if ("reqjoin" == resultnotif.rows[0].type) {
                    UpdateGroupTable(client, resultnotif.rows[0].fr_uuid, resultnotif.rows[0].gid);
                    UpdateUserGroups(client, resultnotif.rows[0].fr_uuid, resultnotif.rows[0].gid);
                }
                RemoveNotif(client, notifid, res);
            });
    }
    else {
        RemoveNotif(client, notifid, res);
    }
});

function RemoveNotif(client, id, res) {
    client.query('DELETE FROM notif WHERE id=$1',
        [parseInt(id)], function (err, resultnotif) {
            if (err) {
                return console.error('error running query for groups', err);
            }
            res.redirect('/notif');
        });
}

function UpdateGroupTable(client, uuid, gid) {
    client.query('SELECT * FROM mygroups WHERE g_id=$1',
        [gid], function (err, resultgrp) {
            if (err) {
                return console.error('error running query for groups', err);
            }
            if(0 == resultgrp.rows.length) return;
            var members = resultgrp.rows[0].members;
            members.push(uuid);
            client.query('UPDATE mygroups SET members=$1 WHERE g_id=$2',
                [members, gid], function (err, result) {
                    if (err) {
                        return console.error('error running insert query', err);
                    }
                });
        });
}